class Dialogue2 {
  constructor() {
    this.lines = [
      {speaker: "Me", message: "\"Urgh....\""},
      {speaker: "", message: "\"You slowly raise up from the ground and begin clasping the back of your head.\""},
      {speaker: "Me", message: "\"What happened...?\""},
      {speaker: "", message: "\"Slowly focusing your vision, you scan your immediate surroundings to figure out where you are.\""},
      {speaker: "", message: "\"From the looks of things, it looks like the street beyond the alley you would take, but something seems off...\""},
      {speaker: "", message: "\"..?\""},
      {speaker: "", message: "\"The street from the town looks the same, but on closer inspection, it feels... abandoned.\""},
      {speaker: "", message: "\"You also just got out of school, but it's suddenly become nighttime!\""},
      {speaker: "", message: "\"An eerie mist also clouds your vision... when a dark shadow start to loom from just beyond the veil.\""},
      {speaker: "Me", message: "\"Whoah whoah whoah, let's taking it easy now buddy! I don't mean you any harm, I'm just trying to go home!\""},
      {speaker: "", message: "\"From beyond the edge of your vision, a figure steps forth with his hands raised above this hands.\""},
      ]
  }
  getCharacterName() {
    return this.characterName;
  }
  getMessage(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].message;
    }
    else {
      return this.lines[index].message;
    }
  }
  getSpeaker(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].speaker;
    }
    else {
      return this.lines[index].speaker;
    } 
  }

  checkIfOutOfBounds(index){
    if (index < 0) {  // Returns First Item if Beyond First Item
      return 0
    }
    else if (index > this.lines.length - 1) { // Returns Last Item if Beyond Last Item
      return this.lines.length - 1
    }
    else { // Return False, Index is Valid
      return false
    }
  }

  getLinesLength(){
    return this.lines.length
  }
  // getNextLine(chapter, line) {
  //   return this.chapters[chapter][line+1];
  // }
  // getPreviousLine(chapter, line) {
  //   return this.chapters[chapter][line-1];
  // }
}