class Dialogue3 {
  constructor() {
    this.lines = [
      {speaker: "Person", message: "\"Hey man, I'm not here to hurt you! I just want to talk - what the heck is going on?\""},
      {speaker: "", message: "\"Observing the man, you have the feeling he won't cause you any harm, so you drop your guard.\""},
      {speaker: "Me", message: "\"Sorry... it's been a weird day. My name is Renomb, how about you?\""},
      {speaker: "", message: "\"I extend my hand out for a handshake, and the man steps forward and return it.\""},
      {speaker: "", message: "\"I raise my eyebrow unconsciously as I begin to look at the man in more detail.\""},
      {speaker: "", message: "\"...Kanye? What are you doing here in Littlewood Town?\""},
      {speaker: "", message: "\"Kayne shrugs apathetically.\""},
      {speaker: "Kayne", message: "\"I was on my way home after working on a new single in the studio when my car was rammed into.\""},
      {speaker: "Kayne", message: "\"The next time I woke up, I was wandering around in the mist. You're the first person I ran into.\""},
      {speaker: "Me", message: "\"That's really weird! I was walking down an alleyway when I was hit in the back of the head by... something... then I woke up here\""},
      {speaker: "", message: "\"Kanye studies me as if looking for an appropriate response.\""},
      {speaker: "Kayne", message: "\"Man, why would you do that! You should NEVER walk into an alley unless there is absolutely no other way!\""},
      {speaker: "", message: "\"Embarrassed, I look down into the ground in shame when I hear a voice booming above me.\""},
      {speaker: "Mysterious Voice", message: "\"Kanye... it's so good to finally see you. Or rather, it's good to see you again.\""},
      {speaker: "Kayne", message: "\"Again? I ain't never been to this place before?\""},
      {speaker: "Mysterious Voice", message: "\"Oh Kayne... so sweet, so naive. Of course you don't remember, you fled a long time ago.\""},
      {speaker: "Mysterious Voice", message: "\"Or at least, part of you did.\""},
      {speaker: "", message: "\"The mysterious voice starts cackling incessantly.\""},
      {speaker: "", message: "\"Unnerved, Kayne steps forward towards the voice in defiance.\""},
      {speaker: "Kayne", message: "\"How can part of you run away from somewhere! I've always been me, and no one else!\""},
      {speaker: "", message: "\"The mysterious voice stops laughing, and begins to take on a grave and serious tone.\""},
      {speaker: "Mysterious Voice", message: "\"Oh Kayne, how have you not figured this out yet?\""},
      {speaker: "Mysterious Voice", message: "\"Haven't you always felt like something was missing inside of you, but couldn't figure out what?\""},
      {speaker: "", message: "\"Kanye remains silent, but becomes uneasy at the reply.\""},
      {speaker: "Mysterious Voice", message: "\"It's you, Kayne. You've been missing part of yourself.\""},
      {speaker: "", message: "\"The mysterious voice stops talking, and suddenly another figure appears from beyond the mist.\""},
      {speaker: "(Evil?) Kanye(?)", message: "\"Welcome back, me. I miss the Old Kanye.\""},
      ]
  }
  getCharacterName() {
    return this.characterName;
  }
  getMessage(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].message;
    }
    else {
      return this.lines[index].message;
    }
  }
  getSpeaker(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].speaker;
    }
    else {
      return this.lines[index].speaker;
    } 
  }

  checkIfOutOfBounds(index){
    if (index < 0) {  // Returns First Item if Beyond First Item
      return 0
    }
    else if (index > this.lines.length - 1) { // Returns Last Item if Beyond Last Item
      return this.lines.length - 1
    }
    else { // Return False, Index is Valid
      return false
    }
  }

  getLinesLength(){
    return this.lines.length
  }
  // getNextLine(chapter, line) {
  //   return this.chapters[chapter][line+1];
  // }
  // getPreviousLine(chapter, line) {
  //   return this.chapters[chapter][line-1];
  // }
}