WL.registerComponent('uiHandler', {
    panel: {type: WL.Type.Enum, values:['simple', 'buttons', 'scrolling', 'images', 'input-text', 'menu', 'scene1', 'scene2','scene3','ending'], default: 'simple'},
    sceneLoadMenu: {type: WL.Type.String, default: '*.bin'},
}, {
    init: function() {
    },
    start: function() {
        this.target = this.object.getComponent('cursor-target');
        this.target.addHoverFunction(this.onHover.bind(this));
        this.target.addUnHoverFunction(this.onUnHover.bind(this));
        this.target.addMoveFunction(this.onMove.bind(this));
        this.target.addDownFunction(this.onDown.bind(this));
        this.target.addUpFunction(this.onUp.bind(this));
        
        this.soundClick = this.object.addComponent('howler-audio-source', {src: 'sfx/click.wav', spatial: true});
        this.soundUnClick = this.object.addComponent('howler-audio-source', {src: 'sfx/unclick.wav', spatial: true});

        // Dialogue Class
        this.dialogue1 = new Dialogue1()
        this.dialogue2 = new Dialogue2()
        this.dialogue3 = new Dialogue3()
        this.ending = new Ending()
    

        switch ( this.panel ){
            case 0://simple
            this.simplePanel();
            break;
            case 1://buttons
            this.buttonsPanel(this.dialogue);
            break;
            case 2://scrolling
            this.scrollPanel();
            break;
            case 3://images
            this.imagePanel();
            break;
            case 4://input-text
            this.inputTextPanel();
            break;
            case 5://menu-opening
            this.menuPanel();
            break;
            case 6://menu
            this.buttonsPanelScene1(this.dialogue1);
            break;
            case 7://menu
            this.buttonsPanelScene2(this.dialogue2);
            break;
            case 8://menu
            this.buttonsPanelScene3(this.dialogue3);
            break;
            case 9://menu
            this.buttonsPanelEnding(this.ending);
            break;
        }
    },
    simplePanel: function(){
        const config = {
            header:{
                type: "text",
                position:{ top:0 },
                paddingTop: 30,
                height: 70
            },
            main:{
                type: "text",
                position:{ top:70 },
                height: 372, // default height is 512 so this is 512 - header height (70) - footer height (70)
                backgroundColor: "#bbb",
                fontColor: "#000"
            },
            footer:{
                type: "text",
                position:{ bottom:0 },
                paddingTop: 30,
                height: 70
            }
        }
        
        const content = {
            header: "Header",
            main: "This is the main text",
            footer: "Footer"
        }
        
        this.ui = new CanvasUI( content, config, this.object );
        this.ui.update();
        let ui = this.ui;
    },
    buttonsPanel: function(dialogue){
        var lineIndex = -1; // Next/Continue Advances to Index 0
        function onPrev(){
            lineIndex += -1;
            if (lineIndex < 0){lineIndex = 0;}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onStop(){
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onNext(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){lineIndex = dialogue.getLinesLength() - 1}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onContinue(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){lineIndex = dialogue.getLinesLength() - 1}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        //console.log('start() with param', this.param);
        const config = {
            panelSize: {
                width: 1,
                height: 1
            },
            height: 512,
            info: {
                type: "text",
                position:{ left: 6, top: 70 },
                width: 500,
                height: 370,
                backgroundColor: "#aaa",
                fontColor: "#000"
            },
            prev: {
                type: "button",
                position:{ top: 445, left: 0 }, 
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onPrev
            },
            stop: {
                type: "button",
                position:{ top: 445, left: 64 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onStop
            },
            next: {
                type: "button",
                position:{ top: 445, left: 128 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onNext
            },
            continue: {
                type: "button",
                position:{ top: 445, right: 10 },
                width: 200,
                height: 52,
                fontColor: "#fff",
                backgroundColor: "#1bf",
                hover: "#3df",
                onSelect: onContinue
            },
            header:{
                type: "text",
                position:{ top:0 },
                paddingTop: 30,
                height: 70
            },
            // main:{
            //     type: "text",
            //     position:{ top:70 },
            //     height: 372, // default height is 512 so this is 512 - header height (70) - footer height (70)
            //     backgroundColor: "#bbb",
            //     fontColor: "#000"
            // },
            // footer:{
            //     type: "text",
            //     position:{ bottom:0 },
            //     paddingTop: 30,
            //     height: 70
            // },
        }
        
        const content = {
            info: "",
            prev: "<path>M 10 32 L 54 10 L 54 54 Z</path>",
            stop: "<path>M 50 15 L 15 15 L 15 50 L 50 50 Z<path>",
            next: "<path>M 54 32 L 10 10 L 10 54 Z</path>",
            continue: "Continue",
            header: "Press continue to begin",
        }

        this.ui = new CanvasUI(content, config, this.object);
        this.ui.update();
        let ui = this.ui;
    },
    buttonsPanelScene1: function(dialogue){
        var lineIndex = -1; // Next/Continue Advances to Index 0
        function onPrev(){
            lineIndex += -1;
            if (lineIndex < 0){lineIndex = 0;}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onStop(){
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onNext(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){WL.scene.load("KQS2.bin");} // Loads Next Scene
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onContinue(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){WL.scene.load("KQS2.bin");} // Loads Next Scene
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        //console.log('start() with param', this.param);
        const config = {
            panelSize: {
                width: 1,
                height: 1
            },
            height: 512,
            info: {
                type: "text",
                position:{ left: 6, top: 70 },
                width: 500,
                height: 370,
                backgroundColor: "#aaa",
                fontColor: "#000"
            },
            prev: {
                type: "button",
                position:{ top: 445, left: 0 }, 
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onPrev
            },
            stop: {
                type: "button",
                position:{ top: 445, left: 64 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onStop
            },
            next: {
                type: "button",
                position:{ top: 445, left: 128 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onNext
            },
            continue: {
                type: "button",
                position:{ top: 445, right: 10 },
                width: 200,
                height: 52,
                fontColor: "#fff",
                backgroundColor: "#1bf",
                hover: "#3df",
                onSelect: onContinue
            },
            header:{
                type: "text",
                position:{ top:0 },
                paddingTop: 30,
                height: 70
            },
            // main:{
            //     type: "text",
            //     position:{ top:70 },
            //     height: 372, // default height is 512 so this is 512 - header height (70) - footer height (70)
            //     backgroundColor: "#bbb",
            //     fontColor: "#000"
            // },
            // footer:{
            //     type: "text",
            //     position:{ bottom:0 },
            //     paddingTop: 30,
            //     height: 70
            // },
        }
        
        const content = {
            info: "",
            prev: "<path>M 10 32 L 54 10 L 54 54 Z</path>",
            stop: "<path>M 50 15 L 15 15 L 15 50 L 50 50 Z<path>",
            next: "<path>M 54 32 L 10 10 L 10 54 Z</path>",
            continue: "Continue",
            header: "Press continue to begin",
        }

        this.ui = new CanvasUI(content, config, this.object);
        this.ui.update();
        let ui = this.ui;
    },
    buttonsPanelScene2: function(dialogue){
        var lineIndex = -1; // Next/Continue Advances to Index 0
        function onPrev(){
            lineIndex += -1;
            if (lineIndex < 0){lineIndex = 0;}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onStop(){
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onNext(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){WL.scene.load("KQS3.bin");} // Loads Next Scene
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onContinue(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){WL.scene.load("KQS3.bin");} // Loads Next Scene
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        //console.log('start() with param', this.param);
        const config = {
            panelSize: {
                width: 1,
                height: 1
            },
            height: 512,
            info: {
                type: "text",
                position:{ left: 6, top: 70 },
                width: 500,
                height: 370,
                backgroundColor: "#aaa",
                fontColor: "#000"
            },
            prev: {
                type: "button",
                position:{ top: 445, left: 0 }, 
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onPrev
            },
            stop: {
                type: "button",
                position:{ top: 445, left: 64 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onStop
            },
            next: {
                type: "button",
                position:{ top: 445, left: 128 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onNext
            },
            continue: {
                type: "button",
                position:{ top: 445, right: 10 },
                width: 200,
                height: 52,
                fontColor: "#fff",
                backgroundColor: "#1bf",
                hover: "#3df",
                onSelect: onContinue
            },
            header:{
                type: "text",
                position:{ top:0 },
                paddingTop: 30,
                height: 70
            },
            // main:{
            //     type: "text",
            //     position:{ top:70 },
            //     height: 372, // default height is 512 so this is 512 - header height (70) - footer height (70)
            //     backgroundColor: "#bbb",
            //     fontColor: "#000"
            // },
            // footer:{
            //     type: "text",
            //     position:{ bottom:0 },
            //     paddingTop: 30,
            //     height: 70
            // },
        }
        
        const content = {
            info: "",
            prev: "<path>M 10 32 L 54 10 L 54 54 Z</path>",
            stop: "<path>M 50 15 L 15 15 L 15 50 L 50 50 Z<path>",
            next: "<path>M 54 32 L 10 10 L 10 54 Z</path>",
            continue: "Continue",
            header: "Press continue to begin",
        }

        this.ui = new CanvasUI(content, config, this.object);
        this.ui.update();
        let ui = this.ui;
    },
    buttonsPanelScene3: function(dialogue){
        var lineIndex = -1; // Next/Continue Advances to Index 0
        function onPrev(){
            lineIndex += -1;
            if (lineIndex < 0){lineIndex = 0;}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onStop(){
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onNext(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){WL.scene.load("KQEnding.bin");} // Loads Next Scene
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onContinue(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){WL.scene.load("KQEnding.bin");} // Loads Next Scene
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        //console.log('start() with param', this.param);
        const config = {
            panelSize: {
                width: 1,
                height: 1
            },
            height: 512,
            info: {
                type: "text",
                position:{ left: 6, top: 70 },
                width: 500,
                height: 370,
                backgroundColor: "#aaa",
                fontColor: "#000"
            },
            prev: {
                type: "button",
                position:{ top: 445, left: 0 }, 
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onPrev
            },
            stop: {
                type: "button",
                position:{ top: 445, left: 64 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onStop
            },
            next: {
                type: "button",
                position:{ top: 445, left: 128 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onNext
            },
            continue: {
                type: "button",
                position:{ top: 445, right: 10 },
                width: 200,
                height: 52,
                fontColor: "#fff",
                backgroundColor: "#1bf",
                hover: "#3df",
                onSelect: onContinue
            },
            header:{
                type: "text",
                position:{ top:0 },
                paddingTop: 30,
                height: 70
            },
            // main:{
            //     type: "text",
            //     position:{ top:70 },
            //     height: 372, // default height is 512 so this is 512 - header height (70) - footer height (70)
            //     backgroundColor: "#bbb",
            //     fontColor: "#000"
            // },
            // footer:{
            //     type: "text",
            //     position:{ bottom:0 },
            //     paddingTop: 30,
            //     height: 70
            // },
        }
        
        const content = {
            info: "",
            prev: "<path>M 10 32 L 54 10 L 54 54 Z</path>",
            stop: "<path>M 50 15 L 15 15 L 15 50 L 50 50 Z<path>",
            next: "<path>M 54 32 L 10 10 L 10 54 Z</path>",
            continue: "Continue",
            header: "Press continue to begin",
        }

        this.ui = new CanvasUI(content, config, this.object);
        this.ui.update();
        let ui = this.ui;
    },
    buttonsPanelEnding: function(dialogue){
        var lineIndex = -1; // Next/Continue Advances to Index 0
        function onPrev(){
            lineIndex += -1;
            if (lineIndex < 0){lineIndex = 0;}
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onStop(){
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onNext(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){
                window.open(`https://twitter.com/oscoxr`, '_blank')
                // WL.scene.load("KQOpening.bin"); // Loads Opening Scene
            } 
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        
        function onContinue(){
            lineIndex += 1;
            if (lineIndex > dialogue.getLinesLength() - 1){
                window.open(`https://twitter.com/oscoxr`, '_blank')
                // WL.scene.load("KQOpening.bin"); // Loads Opening Scene
            }             
            const msg = dialogue.getMessage(lineIndex);
            const name = dialogue.getSpeaker(lineIndex);
            console.log(name + ": " + msg);
            ui.updateElement( "info",   msg );
            ui.updateElement( "header", name );
        }
        //console.log('start() with param', this.param);
        const config = {
            panelSize: {
                width: 1,
                height: 1
            },
            height: 512,
            info: {
                type: "text",
                position:{ left: 6, top: 70 },
                width: 500,
                height: 370,
                backgroundColor: "#aaa",
                fontColor: "#000"
            },
            prev: {
                type: "button",
                position:{ top: 445, left: 0 }, 
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onPrev
            },
            stop: {
                type: "button",
                position:{ top: 445, left: 64 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onStop
            },
            next: {
                type: "button",
                position:{ top: 445, left: 128 },
                width: 64,
                fontColor: "#bb0",
                hover: "#ff0",
                onSelect: onNext
            },
            continue: {
                type: "button",
                position:{ top: 445, right: 10 },
                width: 200,
                height: 52,
                fontColor: "#fff",
                backgroundColor: "#1bf",
                hover: "#3df",
                onSelect: onContinue
            },
            header:{
                type: "text",
                position:{ top:0 },
                paddingTop: 30,
                height: 70
            },
            // main:{
            //     type: "text",
            //     position:{ top:70 },
            //     height: 372, // default height is 512 so this is 512 - header height (70) - footer height (70)
            //     backgroundColor: "#bbb",
            //     fontColor: "#000"
            // },
            // footer:{
            //     type: "text",
            //     position:{ bottom:0 },
            //     paddingTop: 30,
            //     height: 70
            // },
        }
        
        const content = {
            info: "",
            prev: "<path>M 10 32 L 54 10 L 54 54 Z</path>",
            stop: "<path>M 50 15 L 15 15 L 15 50 L 50 50 Z<path>",
            next: "<path>M 54 32 L 10 10 L 10 54 Z</path>",
            continue: "Continue",
            header: "Press continue to begin",
        }

        this.ui = new CanvasUI(content, config, this.object);
        this.ui.update();
        let ui = this.ui;
    },
    scrollPanel: function(){
        const config = {
            body: {
                backgroundColor: "#666"
            },
            txt: {
                type: "text",
                overflow: "scroll",
                position: { left: 20, top: 20 },
                width: 460,
                height: 400,
                backgroundColor: "#fff",
                fontColor: "#000"
            }
        }
        
        const content = {
            txt: "This is an example of a scrolling panel. Select it with a controller and move the controller while keeping the select button pressed. In an AR app just press and drag. If a panel is set to scroll and the overflow setting is 'scroll', then a scroll bar will appear when the panel is active. But to scroll you can just drag anywhere on the panel. This is an example of a scrolling panel. Select it with a controller and move the controller while keeping the select button pressed. In an AR app just press and drag. If a panel is set to scroll and the overflow setting is 'scroll', then a scroll bar will appear when the panel is active. But to scroll you can just drag anywhere on the panel."
        }
        
        this.ui = new CanvasUI( content, config, this.object );
        this.ui.update();
        let ui = this.ui;
    },
    imagePanel: function(){
        const config = {
            panelSize: {
                width: 1,
                height: 1
            },
            image: {
                type: "img",
                position: { left: 20, top: 20 },
                width: 472
            },
            // info: {
            //     type: "text",
            //     position: { top: 300 }
            // }
        }
        
        const content = {
            image: "images/char-kayne-west.jpg",
            // info: "The promo image from the course: Learn to create WebXR, VR and AR, experiences using Wonderland Engine"
        }
        
        this.ui = new CanvasUI( content, config, this.object );
        this.ui.update();
        let ui = this.ui;
    }, 
    inputTextPanel: function(){
        function onChanged( txt ){
            console.log( `message changed: ${txt}`);
        }
        
        function onEnter( txt ){
            console.log(`message enter: ${txt}`);
        }
        
        const config = {
            panelSize: { width: 1, height: 0.25 },
            height: 128,
            message: {
                type: "input-text",
                position: { left: 10, top: 8 },
                height: 56,
                width: 492,
                backgroundColor: "#ccc",
                fontColor: "#000",
                onChanged,
                onEnter
            },
            label: {
                type: "text",
                position: { top: 64 }
            }
        }
        
        const content = {
            message: "",
            label: "Select the panel above."
        }
        
        this.ui = new CanvasUI( content, config, this.object );

        const target = this.ui.keyboard.object.getComponent('cursor-target');
        target.addHoverFunction(this.onHoverKeyboard.bind(this));
        target.addUnHoverFunction(this.onUnHoverKeyboard.bind(this));
        target.addMoveFunction(this.onMoveKeyboard.bind(this));
        target.addDownFunction(this.onDown.bind(this));
        target.addUpFunction(this.onUpKeyboard.bind(this));

        this.ui.update();
        let ui = this.ui;
    }, 
    menuPanel: function(){
        function onContinue(){
            const msg = "Link Start!";
            console.log(msg);
            ui.updateElement( "info", msg );
            WL.scene.load("KQS1.bin");
        }
        //console.log('start() with param', this.param);
        const config = {
            panelSize: {
                width: 1,
                height: 0.25
            },
            height: 128,
            info: {
                type: "text",
                position:{ left: 6, top: 6 },
                width: 500,
                height: 58,
                backgroundColor: "#aaa",
                fontColor: "#000"
            },
            continue: {
                type: "button",
                position:{ top: 70, right: 10 },
                width: 200,
                height: 52,
                fontColor: "#fff",
                backgroundColor: "#1bf",
                hover: "#3df",
                onSelect: onContinue
            }
        }
        
        const content = {
            info: "Kanye Questsona 6 (by OscoXR)",
            continue: "Start"
        }

        this.ui = new CanvasUI( content, config, this.object );
        this.ui.update();
        let ui = this.ui;
    },
    onHover: function(_, cursor) {
        //console.log('onHover');
        if (this.ui){
            const xy = this.ui.worldToCanvas(cursor.cursorPos);
            this.ui.hover(0, xy);
        }

        if(cursor.type == 'finger-cursor') {
            this.onDown(_, cursor);
        }

        this.hapticFeedback(cursor.object, 0.5, 50);
    },

    onMove: function(_, cursor) {
        if (this.ui){
            const xy = this.ui.worldToCanvas(cursor.cursorPos);
            this.ui.hover(0, xy);
        }

        this.hapticFeedback(cursor.object, 0.5, 50);
    },

    onDown: function(_, cursor) {
        console.log('onDown');
        this.soundClick.play();

        this.hapticFeedback(cursor.object, 1.0, 20);
    },

    onUp: function(_, cursor) {
        console.log('onUp');
        this.soundUnClick.play();

        if (this.ui) this.ui.select( 0, true );

        this.hapticFeedback(cursor.object, 0.7, 20);
    },

    onUnHover: function(_, cursor) {
        console.log('onUnHover');
        
        if (this.ui) this.ui.hover(0);

        this.hapticFeedback(cursor.object, 0.3, 50);
    },

    onHoverKeyboard: function(_, cursor) {
        //console.log('onHover');
        if (!this.ui || !this.ui.keyboard || !this.ui.keyboard.keyboard) return;

        const ui = this.ui.keyboard.keyboard;
        const xy = ui.worldToCanvas(cursor.cursorPos);
        ui.hover(0, xy);

        if(cursor.type == 'finger-cursor') {
            this.onDown(_, cursor);
        }

        this.hapticFeedback(cursor.object, 0.5, 50);
    },

    onMoveKeyboard: function(_, cursor) {
        if (!this.ui || !this.ui.keyboard || !this.ui.keyboard.keyboard) return;

        const ui = this.ui.keyboard.keyboard;
        const xy = ui.worldToCanvas(cursor.cursorPos);
        ui.hover(0, xy);

        this.hapticFeedback(cursor.object, 0.5, 50);
    },

    onUpKeyboard: function(_, cursor) {
        console.log('onUpKeyboard');
        this.soundUnClick.play();

        if (this.ui && this.ui.keyboard && this.ui.keyboard.keyboard) this.ui.keyboard.keyboard.select(0);

        this.hapticFeedback(cursor.object, 0.7, 20);
    },

    onUnHoverKeyboard: function(_, cursor) {
        console.log('onUnHoverKeyboard');
        
        if (this.ui && this.ui.keyboard && this.ui.keyboard.keyboard) this.ui.keyboard.keyboard.hover(0);

        this.hapticFeedback(cursor.object, 0.3, 50);
    },

    hapticFeedback: function(object, strength, duration) {
        const input = object.getComponent('input');
        if(input && input.xrInputSource) {
            const gamepad = input.xrInputSource.gamepad;
            if(gamepad && gamepad.hapticActuators) gamepad.hapticActuators[0].pulse(strength, duration);
        }
    },
    
    update: function(dt) {
        //console.log('update() with delta time', dt);
        this.ui.update();
    },
});
