// CLI
// node js/txt/txt-parser.js 

// Globals
let prefix = './js/txt/'
let sceneName = "scene3"
let configInput = prefix + "config.json"
let sceneInput = prefix + "input/" + sceneName +".txt"
let sceneOutput = prefix + "output/" + sceneName +".json"

// Imports
const fs = require('fs');

// Get config file
const configFS = fs.readFileSync(configInput);
const config = JSON.parse(configFS);

// Create a new FileReader object
let text = fs.readFileSync(sceneInput, 'utf8'); // OLD

// Loop over lines of text to create scene list
let outputContent = "";
let linesSplit = text.split('\n')
let numberOfLines = text.split('\n').length
for (let i = 0; i < numberOfLines; i++) {
  let line = linesSplit[i]
  let speaker = config[line.split(" ")[0]]; // Get first word in line and convert that to the full speaker name as written in config.json
  let message = line.split(" ").slice(1).join(" "); // Get rest of line as message
  message = message.replace(/"/g, "\\\""); // To add double quotes into a string in JavaScript, you can use the backslash escape character. 
  // let sceneLine = {"speaker": speaker, "message": messageSafe} // Dict Values
  message = message.replace(/\n|\r/g,''); // You can use the .replace() method to remove a new line from a string.
  let sceneLine = `{speaker: \"${speaker}\", message: \"${message}\"},` 
  if (i != numberOfLines - 1){ // If you are not the last item in the list, add a new line
    sceneLine += "\n"
  }
  outputContent += sceneLine
}

// Delete Output File if it already exists
// if (fs.existsSync(sceneOutput)) { 
//   fs.unlink(sceneOutput, (err) => { 
//     if (err) {
//       throw err; 
//     }
//     console.log('Output file already exists - File deleted successfully.');
//   });
// }

// Add Beginning of File
fs.writeFile(sceneOutput, "this.lines = [" + '\n', (err) => {
  if (err) {
    console.error(err);
    return;
  }
});

// Add element values
fs.appendFile(sceneOutput, outputContent, (err) => {
  if (err) {
    console.error(err);
    return;
  }
});

// Add End of File
fs.appendFile(sceneOutput, "\n]", (err) => {
  if (err) {
    console.error(err);
    return;
  }
});
console.log('File saved successfully!');