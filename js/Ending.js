class Ending {
  constructor() {
    this.lines = [
      {speaker: "Myujen", message: "Thanks for trying out the demo for Myujen, a platform for creating VR worlds in just one sentence. For feedback, suggestions, and updates, follow @OscoXR on Twitter! (Pressing continue will open up the Twitter page)"},
      // {speaker: "Thanks for playing!", message: "We look forward to hearing back from you soon! :^)"},
    ]
  }
  getCharacterName() {
    return this.characterName;
  }
  getMessage(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].message;
    }
    else {
      return this.lines[index].message;
    }
  }
  getSpeaker(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].speaker;
    }
    else {
      return this.lines[index].speaker;
    } 
  }

  checkIfOutOfBounds(index){
    if (index < 0) {  // Returns First Item if Beyond First Item
      return 0
    }
    else if (index > this.lines.length - 1) { // Returns Last Item if Beyond Last Item
      return this.lines.length - 1
    }
    else { // Return False, Index is Valid
      return false
    }
  }

  getLinesLength(){
    return this.lines.length
  }
  // getNextLine(chapter, line) {
  //   return this.chapters[chapter][line+1];
  // }
  // getPreviousLine(chapter, line) {
  //   return this.chapters[chapter][line-1];
  // }
}