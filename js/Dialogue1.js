class Dialogue1 {
  constructor() {
    this.lines = [
      {speaker: "Me", message: "\"What a gorgeous day outside!\""},
      {speaker: "", message: "\"You find yourself stretching after a long day in class.\""},
      {speaker: "Me", message: "\"Man, what a bore! But now it's all over, which means I can go home and start listening to hip-hop!\""},
      {speaker: "", message: "\"You begin walking home throught market street like you always do, you today you can't help but notice an alleyway that wasn't always there.\""},
      {speaker: "Me", message: "\"Huh, isn't that weird? I've walked down this street hundreds of times, but I haven't seen this before.\""},
      {speaker: "Me", message: "\"It's weirdly convenient too, it passes through the market district straight to my neighborhood without having to go around!\""},
      {speaker: "", message: "Lucky! Looks like you'll get home in no time.\""},
      {speaker: "", message: "\"You start moving down the alley, when suddenly a shadow darts from the corner of your eye.\""},
      {speaker: "Me", message: "\"He-\","},
      {speaker: "", message: "\"You begin to say, when an insane pain slams into the back of your head.\""},
      {speaker: "Me", message: "\"Urgh...\""},
      {speaker: "", message: "\"You are unable to finish your sentence as you begin tumbling into the ground as your vision fades to black.\""},
      ]
  }
  getCharacterName() {
    return this.characterName;
  }
  getMessage(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].message;
    }
    else {
      return this.lines[index].message;
    }
  }
  getSpeaker(index) {
    var updatedIndex = this.checkIfOutOfBounds(index)
    if (updatedIndex){
      return this.lines[updatedIndex].speaker;
    }
    else {
      return this.lines[index].speaker;
    } 
  }

  checkIfOutOfBounds(index){
    if (index < 0) {  // Returns First Item if Beyond First Item
      return 0
    }
    else if (index > this.lines.length - 1) { // Returns Last Item if Beyond Last Item
      return this.lines.length - 1
    }
    else { // Return False, Index is Valid
      return false
    }
  }

  getLinesLength(){
    return this.lines.length
  }
  // getNextLine(chapter, line) {
  //   return this.chapters[chapter][line+1];
  // }
  // getPreviousLine(chapter, line) {
  //   return this.chapters[chapter][line-1];
  // }
}